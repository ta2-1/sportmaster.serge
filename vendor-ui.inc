job-template
{
    name                          JSON file processing ('master' branch)
    id                            vendor-ui.json.master # master job id
    db_namespace                  vendor-ui

    db_source                   DBI:SQLite:dbname=./db/vendor-ui.db3

    source_language               ru
    destination_languages         en zh-Hans
    source_match                  ru[^\.]*\.json$


    source_dir                    ./vcs/vendor-ui/master/src/assets/i18n
    output_file_path              ./vcs/vendor-ui/master/src/assets/i18n/%FILE%


    use_keys_as_context           YES
    leave_untranslated_blank      YES
    output_bom                    NO
    output_lang_rewrite
    {
         zh-Hans cn
    }

    parser
    {
        plugin                    parse_json
    }

    ts_file_path                  ./ts/vendor-ui/%LOCALE%/%FILE%.po


    callback_plugins
    {
        :replace_source_lang_with_target_lang
        {
            plugin                replace_strings
            phase                 rewrite_relative_output_file_path
            
            data
            {
                replace    ru %LANG%
                
            }
        }

        :feature_branch
        {
            plugin                feature_branch

            data
            {
                master_job        vendor-ui.json.master  # this must match your master job id
            }
        }
    }
}

# This block will be included conditionally
# for all branches except the `release/` ones (see myproject.cfg).
# This allows one to skip saving localized files in non-release branches
# (but still gather from them strings for translation).
skip-saving-localized-files
{
    callback_plugins
    {
        :skip-saving-localized-files
        {
            plugin                  process_if
            phase                   can_generate_localized_file

            data
            {
                if
                {
                    lang_matches    .

                    then
                    {
                        return      NO
                    }
                }
            }
        }
    }
}
